using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsGenerator : MonoBehaviour
{
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform[] positions;
    List<GameObject> balls = new List<GameObject>();

    public void Generate()
    {
        for (int i = 0; i < positions.Length; i++)
        {
            GameObject ball = Instantiate(ballPrefab, positions[i].position, Quaternion.identity);
            balls.Add(ball);
        }
    }

    public void CleanBalls()
    {
        for(int i = 0; i < balls.Count; i++)
        {
            Destroy(balls[i]);
        }
        balls.Clear();
    }
}
