using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErasableImage : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Collider2D mainCollider;
    [SerializeField] private Elastic elastic;
    [SerializeField] private int interpolCount = 3;
    private Color[] colors;
    private Texture2D newTexture;
    private Texture2D basicTexture;
    private List<Vector2Int> pixelsForErase = new List<Vector2Int>();
    private PolygonCollider2D polygonCollider;
    private Coroutine coroutine;
    private bool needUpdateCollider;

    private void Awake()
    {
        basicTexture = spriteRenderer.sprite.texture;
    }

    private void OnDestroy()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
        Destroy(newTexture);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Elastic"))
        {
            UpdateTexture();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Elastic"))
        {
            UpdateTexture();
        }
    }

    [ContextMenu("Init")]
    public void Init()
    {
        needUpdateCollider = false;
        newTexture = new Texture2D(basicTexture.width, basicTexture.height, TextureFormat.ARGB32, false);
        newTexture.filterMode = FilterMode.Bilinear;
        newTexture.wrapMode = TextureWrapMode.Clamp;
        colors = basicTexture.GetPixels();
        newTexture.SetPixels(colors);
        newTexture.Apply();
        ApplyTexture(newTexture);
        if (polygonCollider != null)
        {
            Destroy(polygonCollider);
        }
        polygonCollider = spriteRenderer.gameObject.AddComponent<PolygonCollider2D>();
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
        coroutine = StartCoroutine(UpdateColliderCoroutine());
    }

    private void UpdateTexture()
    {
        int width = newTexture.width;
        int height = newTexture.height;
        for (int c = 1; c < interpolCount + 2; c++)
        {
            var elasticPos = elastic.LastPos + (elastic.CurrentPos- elastic.LastPos) * c / (interpolCount + 1) - (Vector2)mainCollider.bounds.min;
            elasticPos.x *= width / mainCollider.bounds.size.x;
            elasticPos.y *= height / mainCollider.bounds.size.y;
            Vector2Int pos = new Vector2Int((int)elasticPos.x, (int)elasticPos.y);

            for (int i = 0; i < elastic.Colors.GetLength(0); i++)
            {
                for (int j = 0; j < elastic.Colors.GetLength(1); j++)
                {
                    if (pos.x + i < 0 || pos.x + i >= width || pos.y + j < 0 || pos.y + j >= height) continue;
                    if (elastic.Colors[i, j] == true)
                    {
                        if (pixelsForErase.Contains(new Vector2Int(pos.x + i, pos.y + j)) == false)
                        {
                            colors[(pos.x + i) + (pos.y + j) * width] = elastic.Colors[i, j] ? Color.clear : colors[(pos.x + i) + (pos.y + j) * width];
                            needUpdateCollider = true;
                        }
                    }
                }
            }
        }
        newTexture.SetPixels(colors);
        newTexture.Apply(false, false);
        ApplyTexture(newTexture);
    }

    private void UpdateCollider()
    {

        if (polygonCollider != null)
        {
            if (IsPentaCollider())
            {
                Destroy(polygonCollider);
                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                    coroutine = null;
                }
            }
            else
            {
                Destroy(polygonCollider);
                polygonCollider = spriteRenderer.gameObject.AddComponent<PolygonCollider2D>();
            }
        }
    }

    private bool IsPentaCollider()
    {
        if (polygonCollider.points.Length == 5)
        {
            float d01 = Vector2.Distance(polygonCollider.points[0], polygonCollider.points[1]);
            float d02 = Vector2.Distance(polygonCollider.points[1], polygonCollider.points[2]);
            float d03 = Vector2.Distance(polygonCollider.points[2], polygonCollider.points[3]);
            float d04 = Vector2.Distance(polygonCollider.points[3], polygonCollider.points[4]);
            float d05 = Vector2.Distance(polygonCollider.points[4], polygonCollider.points[0]);
            return Math.Round(d01, 2) == Math.Round(d02, 2) && Math.Round(d02, 2) == Math.Round(d03, 2) && Math.Round(d03, 2) == Math.Round(d04, 2) && Math.Round(d04, 2) == Math.Round(d05, 2) && Math.Round(d05, 2) == Math.Round(d01, 2);
        }
        else
        {
            return false;
        }
    }

    private void ApplyTexture(Texture2D texture)
    {
        spriteRenderer.sprite = Sprite.Create(texture, spriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f), 100, 0u, 0, Vector4.zero, true);
    }

    IEnumerator UpdateColliderCoroutine()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0, 0.35f));
        while (true)
        {
            if (needUpdateCollider)
            {
                UpdateCollider();
                needUpdateCollider = false;
            }
            yield return new WaitForSeconds(0.35f);
        }
    }
}
