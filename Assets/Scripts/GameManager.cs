using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private BallsGenerator ballsGenerator;
    [SerializeField] private ErasableImage[] erasableImages;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button ballsButton;

    private void Start()
    {
        for (int i = 0; i < erasableImages.Length; i++)
        {
            erasableImages[i].Init();
        }
        restartButton.onClick.AddListener(Restart);
        ballsButton.onClick.AddListener(GenerateBalls);
    }

    private void OnDestroy()
    {
        restartButton.onClick.RemoveListener(Restart);
        ballsButton.onClick.RemoveListener(GenerateBalls);
    }

    private void Restart()
    {
        ballsGenerator.CleanBalls();
        for (int i = 0; i < erasableImages.Length; i++)
        {
            erasableImages[i].Init();
        }
    }

    private void GenerateBalls()
    {
        ballsGenerator.Generate();
    }
}
