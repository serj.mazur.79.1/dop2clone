using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elastic : MonoBehaviour
{
    public bool[,] Colors { get; private set; }
    public bool IsActive { get; private set; }
    public Vector2 CurrentPos { get; private set; }
    public Vector2 LastPos { get; private set; }
    public bool LastActiveState { get; private set; }
    [SerializeField] private GameObject elastic;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Collider2D coll;

    private void Start()
    {
        Sprite s = spriteRenderer.sprite;
        Colors = new bool[s.texture.width, s.texture.height];

        for (int i = 0; i < s.texture.width; i++)
        {
            for (int j = 0; j < s.texture.height; j++)
            {
                Colors[i, j] = s.texture.GetPixel(i, j).a != 0;
            }
        }
    }

    private void Update()
    {
        if (LastActiveState == false && Input.GetMouseButton(0) && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;
        IsActive = Input.GetMouseButton(0);
        elastic.SetActive(IsActive);
        if(IsActive)
        {
            if (LastActiveState)
            {
                LastPos = elastic.transform.position - coll.bounds.size * 0.5f;
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 dir = mousePos - elastic.transform.position;
                dir.Normalize();
                Vector2 futurePos = (Vector2)elastic.transform.position + new Vector2(spriteRenderer.sprite.texture.width, spriteRenderer.sprite.texture.height) * dir * 0.15f * Time.deltaTime;
                if (Vector2.Distance(mousePos, elastic.transform.position) < Vector2.Distance(elastic.transform.position, futurePos))
                {
                    mousePos.z = 0;
                    elastic.transform.position = mousePos;
                    CurrentPos = elastic.transform.position - coll.bounds.size * 0.5f;
                }
                else
                {
                    elastic.transform.position = futurePos;
                    CurrentPos = elastic.transform.position - coll.bounds.size * 0.5f;
                }
            }
            else
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                pos.z = 0;
                LastPos = elastic.transform.position - coll.bounds.size * 0.5f;
                CurrentPos = elastic.transform.position - coll.bounds.size * 0.5f;
                elastic.transform.position = pos;
            }
        }
        LastActiveState = IsActive;
    }
}
